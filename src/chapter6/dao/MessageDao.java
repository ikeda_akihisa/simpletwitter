package chapter6.dao;

import static chapter6.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import chapter6.beans.Message;
import chapter6.exception.NoRowsUpdatedRuntimeException;
import chapter6.exception.SQLRuntimeException;

public class MessageDao {

	public void insert(Connection connection, Message message) {

		PreparedStatement ps = null;
		try {
			StringBuilder sql = new StringBuilder();
			sql.append("INSERT INTO messages (");
			sql.append("	user_id, ");
			sql.append("	text, ");
			sql.append("	created_date, ");
			sql.append("updated_date ");
			sql.append(") VALUES ( ");
			sql.append("	?, ");
			sql.append("	?, ");
			sql.append("	CURRENT_TIMESTAMP, ");
			sql.append("	CURRENT_TIMESTAMP ");
			sql.append(")");

			ps = connection.prepareStatement(sql.toString());

			ps.setInt(1,  message.getUserId());
			ps.setString(2, message.getText());

			ps.executeUpdate();

		} catch (SQLException e) {
			throw new SQLRuntimeException(e);
		} finally {
			close(ps);
		}
	}
	public Message select(Connection connection, Integer id) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT * ");
            sql.append("FROM messages ");
        	sql.append("WHERE id = ? ");

            ps = connection.prepareStatement(sql.toString());
        	ps.setInt(1, id );

            ResultSet rs = ps.executeQuery();

            Message message = toMessage(rs);

            return message;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
	public void update(Connection connection, Message message) {

	    PreparedStatement ps = null;
	    try {
	        StringBuilder sql = new StringBuilder();
	        sql.append("UPDATE messages SET ");
	        sql.append("    text = ?, ");
	        sql.append("    updated_date = CURRENT_TIMESTAMP ");
	        sql.append("WHERE id = ?");

	        ps = connection.prepareStatement(sql.toString());

	        ps.setString(1, message.getText());
	        ps.setInt(2, message.getId());

	        int count = ps.executeUpdate();
	        if (count == 0) {
	            throw new NoRowsUpdatedRuntimeException();
	        }
	    } catch (SQLException e) {
	        throw new SQLRuntimeException(e);
	    } finally {
	        close(ps);
	    }
	}
	private Message toMessage(ResultSet rs) throws SQLException {

        Message message = new Message();
        try {
        	if (rs.next()) {
	            message.setId(rs.getInt("id"));
	            message.setUserId(rs.getInt("user_id"));
	            message.setText(rs.getString("text"));
	            message.setCreatedDate(rs.getTimestamp("created_date"));
	            message.setUpdatedDate(rs.getTimestamp("updated_date"));
	            return message;
        	}
        	return null;

        } finally {
            close(rs);
        }
    }


}
