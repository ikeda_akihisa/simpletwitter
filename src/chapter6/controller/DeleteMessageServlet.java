package chapter6.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/deleteMessage" })
public class DeleteMessageServlet extends HttpServlet {

	/*
	 *トップ画面に表示されたつぶやきの「削除」ボタンを押下することで呼ばれる
	 * message_idにつぶやきのIDが渡される
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		// つぶやきのIDを取得する
		String messageId = request.getParameter("message_id");

		// 対象のつぶやきを削除する
		new MessageService().delete(messageId);

		// トップ画面へ遷移する
		response.sendRedirect("./");
	}
}