package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Comment;
import chapter6.beans.User;
import chapter6.service.CommentService;

@WebServlet(urlPatterns = { "/comment" })
public class CommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/*
	 * トップ画面の「返信」ボタンを押下することで呼ばれる
	 * message_idにつぶやきのIDが渡される
	 * textに返信のコメントが渡される
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

		// 返信を読みだす
		Comment comment = getComment(request);


		// 返信の検査を行い、不正ならエラーメッセージを表示しトップ画面へ遷移
		// (不正: テキストがnull/空白のみ/改行のみ, テキストが140文字を超えている)
		if (!isValid(comment.getText(), errorMessages)) {
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}

		// 返信を投稿する
		// (text, updated_date)
		new CommentService().insert(comment);

		// トップ画面へ遷移する
		response.sendRedirect("./");
	}

	/*
	 * commentに値をセット
	 */
    private Comment getComment(HttpServletRequest request) throws IOException, ServletException {

    	HttpSession session = request.getSession();
    	Comment comment = new Comment();
    	int userId = ((User) session.getAttribute("loginUser")).getId();
    	int messageId = Integer.parseInt(request.getParameter("message_id"));
    	String text = request.getParameter("text");
    	comment.setUserId(userId);
    	comment.setMessageId(messageId);
    	comment.setText(text);

        return comment;
    }

	/*
	 * doPostから呼ばれる
	 * 不正なtextを検査する
	 */
	private boolean isValid(String text, List<String> errorMessages) {

		if (StringUtils.isBlank(text)) {
			errorMessages.add("メッセージを入力してください");
		} else if (140 < text.length()) {
			errorMessages.add("140文字以下で入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}
