package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/*
	 * トップ画面に表示されたつぶやきの「編集」ボタンを押下することで呼ばれる
	 * message_idにつぶやきのIDが渡される
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

		// つぶやきのIDを取得する
		String messageId = request.getParameter("message_id");

		// つぶやきIDの検査を行い、問題がなければメッセージを取得
		Message editMessage = null;
		if (!StringUtils.isBlank(messageId) && StringUtils.isNumeric(messageId)) {
			editMessage = new MessageService().selectMessage(messageId);
		}

		// つぶやきIDが不正であればトップ画面へ戻りエラー表示
		// (不正: 数字以外, 存在しないつぶやきID)
		if (editMessage == null) {
			errorMessages.add("不正なパラメータが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}

		// 編集画面にてメッセージのつぶやきを読みだすためリクエストスコープへ保存
		request.setAttribute("edit_message", editMessage);

		// 編集画面へ遷移する
		request.getRequestDispatcher("edit.jsp").forward(request, response);
	}

	/*
	 * 編集画面より「更新」ボタンを押下することで呼ばれる
	 * edit_textに変更したテキストが渡される
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {
		HttpSession session = request.getSession();
		List<String> errorMessages = new ArrayList<String>();

		// つぶやきを読みだす
		Message editMessage = getMessage(request);


		// メッセージの検査を行い、不正なら編集画面にてエラーメッセージを表示
		// (不正: テキストがnull/空白のみ/改行のみ, テキストが140文字を超えている)
		if (!isValid(editMessage.getText(), errorMessages)) {
			session.setAttribute("errorMessages", errorMessages);
			request.setAttribute("edit_message", editMessage);
			request.getRequestDispatcher("edit.jsp").forward(request, response);
			return;
		}

		// つぶやきのメッセージを更新する
		// (text, updated_date)
		new MessageService().update(editMessage);

		// トップ画面へ遷移する
		response.sendRedirect("./");
	}

	/*
	 * テキスト編集後のメッセージを読みだす
	 */
    private Message getMessage(HttpServletRequest request) throws IOException, ServletException {

    	String id =  request.getParameter("message_id");
    	Message message = new MessageService().selectMessage(id);
    	message.setText(request.getParameter("edit_text"));

        return message;
    }

	/*
	 * doPostから呼ばれる
	 * 不正なtextを検査する
	 */
	private boolean isValid(String text, List<String> errorMessages) {

		if (StringUtils.isBlank(text)) {
			errorMessages.add("メッセージを入力してください");
		} else if (140 < text.length()) {
			errorMessages.add("140文字以下で入力してください");
		}

		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}
