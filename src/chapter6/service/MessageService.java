package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

	/*
	 * 新しくつぶやきを投稿する
	 */
	public void insert(Message message) {

		Connection connection = null;
		try {
			connection = getConnection();
			new MessageDao().insert(connection, message);
			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
	/*
	 * ユーザーIDから該当ユーザー全てのつぶやきを返す
	 */

	public List<UserMessage> selectMessageList(String userId, String start, String end) {
		final int LIMIT_NUM = 1000;

		Connection connection = null;
		try {
			connection = getConnection();
			Integer id = null;

			// 入力された値を検査する
			if (!StringUtils.isEmpty(userId)) {
				id = Integer.parseInt(userId);
			}

			// startに入力があれば同日00:00:00に変更する
			if (!StringUtils.isBlank(start)) {
				start += " 00:00:00";
			} else {
				start = "2022-02-01 00:00:00"; // 入力がなければ初期設定時刻
			}

			// endに入力があれば同日23:59:59に変更する
			if (!StringUtils.isBlank(end)) {
				end += " 23:59:59";
			} else {
				SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				end = f.format(new Date()); // 入力がなければ現在時刻
			}

			List<UserMessage> messages = new UserMessageDao().select(connection, id, start, end, LIMIT_NUM);
			commit(connection);

			return messages;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/*
	 * つぶやきのIDから該当のつぶやきを返す
	 */
	public Message selectMessage(String messageId) {
		Connection connection = null;
		try {
			connection = getConnection();
			Integer id = null;
			if (!StringUtils.isEmpty(messageId)) {
				id = Integer.parseInt(messageId);
			}
			Message message = new MessageDao().select(connection, id);
			commit(connection);

			return message;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/*
	 * つぶやきのIDから該当のつぶやきを削除する
	 */
	public void delete(String messageId) {

		Connection connection = null;
		try {
			connection = getConnection();
			Integer id = null;
			if (!StringUtils.isEmpty(messageId)) {
				id = Integer.parseInt(messageId);
			}
			new UserMessageDao().delete(connection, id);
			commit(connection);

		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

	/*
	 * つぶやきの更新処理を行う
	 */
	public void update(Message message) {


        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().update(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}
