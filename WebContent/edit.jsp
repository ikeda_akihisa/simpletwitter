<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>つぶやき編集画面</title>
        <link href="css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">
			<!-- エラーメッセージの表示 -->
			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="errorMessage">
							<li><c:out value="${errorMessage}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session" />
			</c:if>
			<!-- つぶやきフォーム、更新ボタンの作成 -->
			<div class="form-area">
				<form action="edit" method="post">
					つぶやき<br />
					<textarea name="edit_text" cols="100" rows="5" class="tweet-box"><c:out value="${edit_message.text}" /></textarea>
					<br />
					<input name="message_id" type="hidden" value="${edit_message.id}">
					<input type="submit" value="更新"><br />
					<a href="./">戻る</a>
				</form>
			</div>
            <div class="copyright"> Copyright(c)Akihisa Ikeda</div>
        </div>
    </body>
</html>